# LibrEDA - Physical Chip Design Framework

## Abstract

The main goal of this project is to create a new libre software framework for the physical design of digital integrated circuits. The framework is meant to simplify the development of ASIC layout tools, i.e. the tools used to convert a gate-level netlist into a fabrication-ready layout. A special focus will be on modularity, reusability, documentation and maintainability. A user who is not familiar with the framework should be quickly able to learn how to use it and how to write extensions. For this the framework will provide fundamental data structures and algorithms,  interface definitions of the design algorithms (e.g. placement, routing or timing analysis), input/output libraries for commonly used file formats (e.g. GDS2, OASIS, DEF) as well as documentation and example implementations for each abstract part.
Two implementations will be pursued in parallel: One with a clear focus on simplicity and education and another with a focus on performance and scalability. The first will be written in Python, the second in a more performance-oriented language (for example Rust).
At the same time the ‘LibreCell’ standard-cell generator and characterization tool will be developed further.

## The framework

The ‘Physical Chip Design Framework’ can be roughly split into the following parts:

* Data structures that represent chip layouts, netlists and the combination of the two.
* Interface definitions of commonly used parts in a chip layout flow:
    * Input/output for layouts & netlists
    * Placement algorithms
    * Routing algorithms
    * Timing analysis with an eventual feedback loop to the routing algorithm

There are also parts that are not yet considered such as a pad-ring generator.
This project persues two different approaches. One is a framework written in Python that can use existing data structures of Klayout (klayout.de). The Python framework is intended to be as simple as possible and not necessarily efficient or scalable. It should be suitable for teaching and tutorials.
The second version should be written in a performance-oriented way (for example in Rust). This version also intents to be scalable for real-world designs.
Obviously there are overlaps in the tasks. Where hard work of another task can be reused this is respected in the requested amount.
Dependency or priority of tasks does has nothing to do with the order of appearance.

## Acknowledgements

Supported by:

![NLNet.nl](./img/nlnet.nl_banner.png)

![NGI0](./img/NGI0_tag.png)
